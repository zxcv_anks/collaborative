from frappe import _

def get_data():
	return [
		{
			"module_name": "Collaborative",
			"type": "module",
			"label": _("Collaborative")
		}
	]
