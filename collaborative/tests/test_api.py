import unittest

import frappe

from collaborative.api import (create_new_project, create_new_task,
                               get_projects_info)


class TestAPI(unittest.TestCase):
	def tearDown(self):
		frappe.db.rollback()  # undo all changes to database

	def test_new_project(self):
		id = "New test project"

		project = create_new_project(id)

		self.assertTrue(frappe.db.exists("Collaborative Project", {"name": project.name}))
		self.addClassCleanup(project.delete)

	def test_create_new_task(self):
		project = create_new_project("project task")
		self.addClassCleanup(project.delete)

		task = create_new_task(project.name, "Test Task")
		self.assertTrue(frappe.db.exists("Collaborative Task", task.name))
		self.addClassCleanup(task.delete)

	def test_get_projects_info(self):
		project = create_new_project("new project info")

		project.append("team_members", {"user": "Administrator"})
		project.save()
		self.addClassCleanup(project.delete)

		info = get_projects_info()

		self.assertIn(project.name, info)
		self.assertEqual(info[project.name]["team_members"][0].name, "Administrator")
