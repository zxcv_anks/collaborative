import frappe


@frappe.whitelist()
def create_new_project(name: str):
	"""Create a new project"""

	project = frappe.new_doc("Collaborative Project")
	project.project_name = name
	return project.save()


@frappe.whitelist()
def create_new_task(project_id: str, description: str):
	"""Create a new tasks and link it with project"""

	task = frappe.new_doc("Collaborative Task")
	task.description = description
	task.project = project_id
	return task.save()


@frappe.whitelist()
def get_projects_info():

	project_info = {}

	for project_id in frappe.get_all("Collaborative Project", pluck="name"):
		project = frappe.get_doc("Collaborative Project", project_id)
		project_info[project_id] = {
			"team_members": [get_user_info(user.user) for user in project.team_members],
			"about": project.about,
		}

	return project_info


def get_user_info(user):
	info = (
		frappe.db.get_value("User", user, ["full_name", "user_image"], as_dict=True)
		or frappe._dict()
	)
	return frappe._dict(
		fullname=info.full_name or user,
		image=info.user_image or "https://i.imgur.com/01tUI1K.png",
		name=user,
	)
