import { createRouter, createWebHistory } from "vue-router";
import Projects from "../views/Projects.vue";
import MyTasks from "../views/MyTasks.vue";
import authRoutes from './auth';

const routes = [
  {
    path: "/project/:project_id",
    name: "Project",
    component: Projects,
    props: true,
  },
  {
    path: "/project",
    name: "ProjectList",
    component: Projects,
  },
  {
    path: "/my-tasks",
    name: "MyTasks",
    component: MyTasks,
  },
  ...authRoutes,
];

const router = createRouter({
  base: "/collaborative/",
  history: createWebHistory(),
  routes,
});

export default router;
