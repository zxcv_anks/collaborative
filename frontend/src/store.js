import { syncedStore, getYjsValue } from "@syncedstore/core";
import { WebrtcProvider } from "y-webrtc";


// TODO:
// - Tasks should keept track of "needs saving"
// - Users should be able to connect / disconnect
// - switch to socketio
// - store should be responsible for commiting the changes.


// Create your SyncedStore store
export const store = syncedStore({ projects: {}, fragment: "xml" });

// Create a document that syncs automatically using Y-WebRTC
const doc = getYjsValue(store);
export const webrtcProvider = new WebrtcProvider("syncedstore-tasks", doc);

export const disconnect = () => webrtcProvider.disconnect();
export const connect = () => webrtcProvider.connect();
